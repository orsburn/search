# README #

This project began as a configurable access point for a collection of search sources.  Over time it grew and slowly assumed new features.  It became time to make it something that is easy to manage and tailor for anyone.

# The quick of it #

Manage three files, to collections of resources and a settings file.

* A services file stores the collection of search services that show up in the top portion of the page.
* An applications file that is the collection of sites that show up in the bottom portion of the page (think bookmarks).
* A settings file to indicate things like the default search site and the default theme.

# Installation #

1. Download the repository (from Downloads link on left)
1. Expand locally
1. Copy the contents of the /unzipped/here/search/dist directory to any directory.
1. Load /local/path/search/index.html in a browser
1. Set this page as the default homepage for your browser.

# Manage files #

### /local/path/search/DB/services.js ###

This is a JavaScript file that initializes an array of objects.  Each object represents a search service like Wikipedia or PHP functions.

```
#!javascript
// Example services.js
{
	service: "Dictionary",
	URL: "http://dictionary.reference.com/",
	searchURL: "http://dictionary.reference.com/browse/[search]",
	icon: "dictionary.ico"
}
```

* **service:**  This is the label for the item that appears in the search service area at the top of the page.
* **URL:**  This is the address for the service.  Clicking on the label for the service will go to the service's page.
* **searchURL:**  The URL that will be loaded to search against the service.  The [search] portion is the placeholder for the search terms for the service.
* **icon:**  The page looks to the icons directory to find an icon for the service.  This is the name of the file it will look for.
* **prepFunction:**  This will be described more later.

### /local/path/search/DB/applications.js ###

```
#!javascript
// Example applications.js
{
	application: "Amazon Music",
	URL: "https://www.amazon.com/gp/dmusic/cloudplayer/player",
	icon: "amazonMusic.jpeg"
}
```
This file is nearly identical in function as that of services.js, only there is no need for the **searchURL** or **prepFunction** values.  Also note that the **services** field is called "application" in this file.

### /local/path/search/settings.js ###

```
#!javascript
{
	defaultService: "DuckDuckGo",
	defaultTheme: "dark",
	servicesColumns: 3,
	applicationsColumns: 3
}
```

* **defaultService:**  Indicates the search service that will be selected by default.
* **defaultTheme:**  This is the color theme that will be loaded by default.
* **servicesColumns:**  The number of columns the listing of search services will be spread over.
* **applicationsColumns:**  The number of columns the listing of applications will be spread over.

# prepFunction #

For some search services it may be necessary to sort of "condition" search terms a bit before passing them to the search service.  An example of this is that of Wikipedia and how it replaces spaces with underscores and capitalizes the first letter, and PHP functions replaces underscores with dashes.

* Wikipedia searches for "corn dogs" as "Corn_dogs"
* PHP functions searches for "str_replace" as "str-replace"

The custom operations that should be performed on search terms can be defined in the **JS/conditioningFunctions.js** file.  Create a function for the service in this file, then use the **prepFunction** field in the **services.js** file to indicate the function name as a string value of the function name.

1. Create function definition in **JS/conditioningFunctions.js**
1. Add reference to function as string value in **JS/services.js** object for service.