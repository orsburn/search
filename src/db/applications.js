applications = [
	{
		application: "Amazon Music",
		URL: "https://www.amazon.com/gp/dmusic/cloudplayer/player",
		icon: "amazonMusic.png"
	},
	{
		application: "Amazon",
		URL: "https://www.amazon.com/",
		icon: "amazon.ico"
	},
	{
		application: "Bookmarks",
		URL: "https://www.google.com/bookmarks",
		icon: "bookmarks.ico"
	},
	{
		application: "Drive",
		URL: "https://drive.google.com/",
		icon: "drive.ico"
	},
	{
		application: "Facebook",
		URL: "https://www.facebook.com/",
		icon: "facebook.ico"
	},
	{
		application: "Feedly",
		URL: "https://feedly.com/",
		icon: "feedly.ico"
	},
	{
		application: "Gmail",
		URL: "https://mail.google.com/",
		icon: "gmail.ico"
	},
	{
		application: "Google Calendar",
		URL: "https://calendar.google.com/calendar/render",
		icon: "googleCalendar.png"
	},
	{
		application: "Google Contacts",
		URL: "https://contacts.google.com/",
		icon: "googleContacts.png"
	},
	{
		application: "Hangouts",
		URL: "https://hangouts.google.com/",
		icon: "hangouts.ico"
	},
	{
		application: "Keep",
		URL: "https://keep.google.com/",
		icon: "keep.ico"
	},
	{
		application: "LinkedIn",
		URL: "https://www.linkedin.com/",
		icon: "linkedIn.ico"
	},
	{
		application: "Notepad",
		URL: "https://calendar.yahoo.com/?view=notepad",
		icon: "notepad.ico"
	},
	{
		application: "Play Radio",
		URL: "https://play.google.com/music/listen",
		icon: "playRadio.png"
	},
	{
		application: "Plus",
		URL: "https://plus.google.com/",
		icon: "plus.ico"
	}
];