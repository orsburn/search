services = [
	{
		service: "Dictionary",
		URL: "http://dictionary.reference.com/",
		searchURL: "http://dictionary.reference.com/browse/[search]",
		icon: "dictionary.ico"
	},
	{
		service: "DuckDuckGo",
		URL: "https://duckduckgo.com/",
		searchURL: "https://duckduckgo.com/?q=[search]",
		icon: "duckduckgo.ico"
	},
	{
		service: "Google",
		URL: "https://www.google.com/",
		searchURL: "http://www.google.com/search?hl=en&btnG=Google+Search&q=[search]",
		icon: "google.ico"
	},
	{
		service: "Google Play",
		URL: "http://play.google.com/store/apps",
		searchURL: "http://play.google.com/store/search?q=[search]&c=apps",
		icon: "googlePlay.ico"
	},
	{
		service: "IMDb",
		URL: "http://www.imdb.com/",
		searchURL: "http://www.imdb.com/find?s=all&q=[search]",
		icon: "imdb.ico"
	},
	{
		service: "iTunes App Store",
		URL: "http://www.apple.com/itunes",
		searchURL: "https://duckduckgo.com/?q=site%3Aitunes.apple.com+[search]",
		icon: "iTunesAppStore.ico"
	},
	{
		service: "PHP Functions",
		URL: "http://www.php.net/",
		searchURL: "http://www.php.net/manual/en/function.[search].php",
		icon: "phpFunctions.ico",
		prepFunction: "phpCondition"
	},
	{
		service: "PHP Search",
		URL: "http://www.php.net/",
		searchURL: "http://www.php.net/results.php?p=manual&q=[search]",
		icon: "php.ico"
	},
	{
		service: "Stack Overflow",
		URL: "http://stackoverflow.com/",
		searchURL: "http://duckduckgo.com/?q=site%3Astackoverflow.com+[search]",
		icon: "stackOverflow.ico"
	},
	{
		service: "Wikipedia",
		URL: "http://www.wikipedia.org/",
		searchURL: "http://en.wikipedia.org/wiki/[search]",
		icon: "wikipedia.ico",
		prepFunction: "wikipediaCondition"
	},
	{
		service: "Yahoo",
		URL: "http://search.yahoo.com/",
		searchURL: "http://search.yahoo.com/search?ei=UTF-8&fr=sfp&p=[search]",
		icon: "yahoo.ico"
	},
	{
		service: "YouTube",
		URL: "https://www.youtube.com/",
		searchURL: "http://www.youtube.com/results?search_type=&search_query=[search]",
		icon: "youTube.ico"
	}
];