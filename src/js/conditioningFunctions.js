function phpCondition(value)
{
	return value.replace(/[\s_]/g, "-");
}


function wikipediaCondition(value)
{
	/* -----------------------------------------------------------------------------
	This bit below will capitalize the first letter of each word, if it's deemed
	that this is the preferred conditioning.  Otherwise, the line below will just
	replace spaces with underscores and capitalize the first letter.

	Scott Orsburn | scott@orsburn.net | 01/31/2016
	----------------------------------------------------------------------------- */
	//	values = value.split(" ");
	//
	//	values = values.map(function(current)
	//	{
	//		working = current.toLowerCase();
	//		return working.charAt(0).toUpperCase() + working.slice(1);
	//	});
	//
	//	return values.join("_");

	value = value.toLowerCase();

	return (value.charAt(0).toUpperCase() + value.slice(1)).replace(/[\s_]/g, "_") ;
}