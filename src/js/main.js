renderSearchServices();
renderApplications();


function renderSearchServices()
{
	var servicesIndex = 0;

	for(var columnIndex = 0; columnIndex < settings.servicesColumns; columnIndex ++)
	{
		var continue_f = true;
		var sectionIndex = 0;
		var template = "<div id=\"servicesSection_" + columnIndex + "\" style=\"float: left; width: " + (100 / settings.servicesColumns) + "%;\">";

		while(continue_f === true)
		{
			if(sectionIndex + 1 === Math.ceil(services.length / settings.servicesColumns)){continue_f = false;}
			if(servicesIndex + 1 === services.length){continue_f = false;}

			template += "<div class=\"service\">\n";
			template += "	<input\n";
			template += "		type=\"radio\"\n";
			template += "		id=\"" + services[servicesIndex].service.replace(/\s/g, "") + "\" name=\"selectedService\"\n";
			template += "		value=\"" + services[servicesIndex].service + "\" />&nbsp;&nbsp;";
			template += "	<img\n";
			template += "		src=\"icons/" + services[servicesIndex].icon + "\"\n";
			template += "		alt=\"" + services[servicesIndex].service + "icon\"\n";
			template += "		height=\"16px;\" width=\"16px;\"\n";
			template += "		onclick=\"document.querySelector('#" + services[servicesIndex].service.replace(/\s/g, "") + "').click();\" />&nbsp;&nbsp;";
			template += "	<a class=\"serviceName\" href=\"" + services[servicesIndex].URL + "\">" + services[servicesIndex].service + "</a>";
			template += "</div>\n";

			sectionIndex ++;
			servicesIndex ++;
		}

		template += "<\div>\n";

		document.querySelector("#services").innerHTML += template;
	}
}


function renderApplications()
{
	var applicationsIndex = 0;

	for(var columnIndex = 0; columnIndex < settings.applicationsColumns; columnIndex ++)
	{
		var continue_f = true;
		var sectionIndex = 0;
		var template = "<div id=\"applicationsSection_" + columnIndex + "\" style=\"float: left; width: " + (100 / settings.applicationsColumns) + "%;\">";

		while(continue_f === true)
		{
			if(sectionIndex + 1 === Math.ceil(applications.length / settings.applicationsColumns)){continue_f = false;}
			if(applicationsIndex + 1 === applications.length){continue_f = false;}

			template += "<div class=\"application\">\n";
			template += "	<img\n";
			template += "		src=\"icons/" + applications[applicationsIndex].icon + "\"\n";
			template += "		alt=\"" + applications[applicationsIndex].application + "icon\"\n";
			template += "		height=\"16px;\" width=\"16px;\"\n";
			template += "		onclick=\"window.location = '" + applications[applicationsIndex].URL + "';\" />&nbsp;&nbsp;";
			template += "	<a class=\"applicationName\" href=\"" + applications[applicationsIndex].URL + "\">" + applications[applicationsIndex].application + "</a>";
			template += "</div>\n";

			sectionIndex ++;
			applicationsIndex ++;
		}

		template += "<\div>\n";

		document.querySelector("#applications").innerHTML += template;
	}
}


defaultService = services.filter(function(current)
{
	if(current.service.toLowerCase() === settings.defaultService.toLowerCase())
	{
		return current.service;
	}
})[0];

document.querySelector("#" + defaultService.service.replace(/\s/g, "")).click();
document.querySelector("#searchInput").select();
document.querySelector("#searchForm").addEventListener("submit", search);  // This is needed since a call to the search() function isn't reachable in the order that files are brought into the application.  (Scott Orsburn | scott@orsburn.net | 01/29/2016)
changeTheme(settings.defaultTheme);

function search()
{
	var locateService = "";
	var inputs = document.getElementsByName("selectedService");

	// Locating the service to search against.
	for(var i = 0; i < inputs.length; i ++)
	{
		if(inputs[i].checked)
		{
			locateService = document.querySelector("#" + inputs[i].id).value;
		}
	}

	// Retrieving the details for the target service to search against.
	targetService = services.filter(function(current)
	{
		if(current.service.toLowerCase() === locateService.toLowerCase())
		{
			return current.service;
		}
	})[0];

	// Prepping the search value and URL.
	var searchValue = document.querySelector("#searchInput").value;

	if(typeof targetService.prepFunction === "string")
	{
		searchValue = window[targetService.prepFunction](searchValue);
	}

	searchValue = encodeURIComponent(searchValue);
	searchURL = targetService.searchURL.replace(/\[search\]/, searchValue);

	window.location = searchURL;

	return false;
}


function changeTheme(themeName)
{
	document.querySelector("#theme").href = "css/" + themeName + ".css";
}