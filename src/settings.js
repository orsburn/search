settings = {
	defaultService: "DuckDuckGo",
	defaultTheme: "dark",
	servicesColumns: 3,
	applicationsColumns: 3
};